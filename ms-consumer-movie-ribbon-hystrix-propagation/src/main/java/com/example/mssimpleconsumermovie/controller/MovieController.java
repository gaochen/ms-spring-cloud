package com.example.mssimpleconsumermovie.controller;

import com.example.mssimpleconsumermovie.entity.User;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.annotation.SessionScope;

@RestController
@SessionScope
public class MovieController {
  @Autowired
  private RestTemplate restTemplate;

/*  @Value("${user.userServicePath}")
  private String userServicePath;

  @GetMapping("/movie/{id}")
  public User findById(@PathVariable Long id) {
    return this.restTemplate.getForObject(this.userServicePath + id, User.class);
  }
  */

  /**
   *  commandProperties = @HystrixProperty(name = "execution.isolation.strategy",value = "SEMAPHORE")
   *  这个配置可以让findById方法和HystrixCommand在一个线程中
   *  否则 默认不配置的时候 HystrixCommand是在一个隔离线程中
   *
   *  注意 这里正常时候不要配置  等到抛异常了在配置
   */
  @GetMapping("/movie/{id}")
  @HystrixCommand(fallbackMethod = "findByIdFallback" , commandProperties = @HystrixProperty(name = "execution.isolation.strategy",value = "SEMAPHORE"))
  public User findById(@PathVariable Long id) {
    //这里不再需要被调用的服务的ip端口 直接用serviceid就可以（一般是spring.application.name）
    return this.restTemplate.getForObject("http://ms-provider-user:7900/simple/" + id, User.class);
  }

  /**
   * hystrix的默认超时时间是1秒钟 可以在yml中配置这个时间
   * hystrix.command.default.execution.isolation.thread.timeoutInMilliseconds: 5000
   */
  public User findByIdFallback(Long id){
    User user = new User();
    user.setId(0L);
    return user;
  }

}
