package com.example.mssimpleconsumermovie.controller;

import com.example.mssimpleconsumermovie.entity.User;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class MovieController {
  @Autowired
  private RestTemplate restTemplate;

  @GetMapping("/movie/{id}")
  @HystrixCommand(fallbackMethod = "findByIdFallback")
  public User findById(@PathVariable Long id) {
    //这里不再需要被调用的服务的ip端口 直接用serviceid就可以（一般是spring.application.name）
    return this.restTemplate.getForObject("http://ms-provider-user:7900/simple/" + id, User.class);
  }

  /**
   * hystrix的默认超时时间是1秒钟 可以在yml中配置这个时间
   * hystrix.command.default.execution.isolation.thread.timeoutInMilliseconds: 5000
   */
  public User findByIdFallback(Long id){
    User user = new User();
    user.setId(0L);
    return user;
  }

}
