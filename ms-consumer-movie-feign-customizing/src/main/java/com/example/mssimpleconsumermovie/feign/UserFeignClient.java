package com.example.mssimpleconsumermovie.feign;

import com.example.configuration.FeignConfiguration;
import com.example.mssimpleconsumermovie.entity.User;
import feign.Param;
import feign.RequestLine;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 *
 */
@FeignClient(name = "ms-provider-user" , configuration = FeignConfiguration.class)
public interface UserFeignClient {
    @RequestLine("GET /simple/{id}")
    public User findById(@Param(value = "id") Long id);
}
