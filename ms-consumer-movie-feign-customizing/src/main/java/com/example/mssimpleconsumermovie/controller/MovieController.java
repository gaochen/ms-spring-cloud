package com.example.mssimpleconsumermovie.controller;

import com.example.mssimpleconsumermovie.entity.User;
import com.example.mssimpleconsumermovie.feign.UserFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class MovieController {

  //这里注入feign 直接调用feign的方法
  @Autowired
  private UserFeignClient userFeignClient ;

  @GetMapping("/movie/{id}")
  public User findById(@PathVariable Long id) {
    return userFeignClient.findById(id);
  }

}
