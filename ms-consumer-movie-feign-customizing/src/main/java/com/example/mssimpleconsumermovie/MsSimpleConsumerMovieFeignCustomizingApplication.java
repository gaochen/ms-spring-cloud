package com.example.mssimpleconsumermovie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

@EnableEurekaClient
@SpringBootApplication
@EnableFeignClients
public class MsSimpleConsumerMovieFeignCustomizingApplication {
    public static void main(String[] args) {
        SpringApplication.run(MsSimpleConsumerMovieFeignCustomizingApplication.class, args);
    }
}
