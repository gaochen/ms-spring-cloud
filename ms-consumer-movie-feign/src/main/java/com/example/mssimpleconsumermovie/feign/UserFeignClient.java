package com.example.mssimpleconsumermovie.feign;

import com.example.mssimpleconsumermovie.entity.User;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * 使用feign调用其它的service 可以不使用restTemplate
 *  这里使用RequestMapping 指定被调用的service中controller的路径
 * 注意：
 * 这里的feign只能用RequestMapping 不能使用GetMapping和PostMapping
 * 这里的PathVariable必须写value
 * 这里的feign的方法参数 只要是复杂的对象 这个方法必须是post 即使feign中指定了get也会报错
 *      可以使用@RequestParam(value = "id") Long id 来接受多个参数
 */
@FeignClient("ms-provider-user")
public interface UserFeignClient {

    @RequestMapping(value = "/simple/{id}" , method = RequestMethod.GET)
    public User findById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/user" , method = RequestMethod.POST)
    public User postUser(@RequestBody User user);

    @RequestMapping(value = "/get-user" , method = RequestMethod.GET)
    public User getUser(@RequestParam(value = "id") Long id ,
                        @RequestParam(value = "username") String username ,@RequestParam(value = "name") String name);
}
