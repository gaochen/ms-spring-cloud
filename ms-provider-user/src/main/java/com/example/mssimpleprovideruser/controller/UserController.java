package com.example.mssimpleprovideruser.controller;

import com.example.mssimpleprovideruser.entity.User;
import com.example.mssimpleprovideruser.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

  @Autowired
  private UserRepository userRepository;

  @GetMapping("/simple/{id}")
  public User findById(@PathVariable Long id){
    System.out.println("-------------------------------------------->from user 1");
    return userRepository.findOne(id);
  }

  @PostMapping("/user")
  public User postUser(@RequestBody User user){
    return user;
  }

  @GetMapping("/get-user")
  public User getUser( User user){
    return user;
  }

}
