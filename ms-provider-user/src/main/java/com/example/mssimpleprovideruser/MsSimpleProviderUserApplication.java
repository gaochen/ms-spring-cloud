package com.example.mssimpleprovideruser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class MsSimpleProviderUserApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsSimpleProviderUserApplication.class, args);
    }
}
