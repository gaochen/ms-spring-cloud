package com.example.mssimpleprovideruser.controller;

import com.example.mssimpleprovideruser.entity.User;
import com.example.mssimpleprovideruser.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

  @Autowired
  private UserRepository userRepository;

  @GetMapping("/simple/{id}")
  public User findById(@PathVariable Long id){
    System.out.println("-------------------------------------------->from user 2");
    return userRepository.findOne(id);
  }


}
