package com.example.msdiscoveryeureka;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;

public class PreZuulFilter extends ZuulFilter {

    private static final Logger logger = LoggerFactory.getLogger(PreZuulFilter.class);

    //filter的类型
    @Override
    public String filterType() {
        return "pre";
    }

    //执行顺序 数字越大越靠后
    @Override
    public int filterOrder() {
        return 1;
    }

    //判断是否要用这个过滤器 事先判断下
    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        HttpServletRequest request = RequestContext.getCurrentContext().getRequest();
        String host = request.getRemoteHost();
        logger.info("--------------------->>>>>"+host);
        return null;
    }
}
