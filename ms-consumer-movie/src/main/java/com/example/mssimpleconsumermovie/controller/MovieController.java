package com.example.mssimpleconsumermovie.controller;

import com.example.mssimpleconsumermovie.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class MovieController {
  @Autowired
  private RestTemplate restTemplate;

/*  @Value("${user.userServicePath}")
  private String userServicePath;

  @GetMapping("/movie/{id}")
  public User findById(@PathVariable Long id) {
    return this.restTemplate.getForObject(this.userServicePath + id, User.class);
  }
  */

  @GetMapping("/movie/{id}")
  public User findById(@PathVariable Long id) {
    //这里不再需要被调用的服务的ip端口 直接用serviceid就可以（一般是spring.application.name）
    return this.restTemplate.getForObject("http://ms-provider-user:7900/simple/" + id, User.class);
  }

}
