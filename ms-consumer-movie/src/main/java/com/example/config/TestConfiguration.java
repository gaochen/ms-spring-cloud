package com.example.config;

import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//主意 @Configuration这个注解不能被 @ComponentScan ,@SpringBootApplication扫描到  否则这个ribbon的配置会被所有公用
@Configuration
public class TestConfiguration {

    //这里注入config会报错 不过没事
    @Autowired
    public IClientConfig config;

    @Bean
    public IRule ribbonRule(IClientConfig config){
        return new RandomRule();
    }
}
